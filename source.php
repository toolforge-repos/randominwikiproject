<?php
/* SPDX-FileCopyrightText:  2020–2023, Alexander Hecht <zanandallison@zanandallison.com>
 * SPDX-FileCopyrightText:  2024, Peter J. Mello <admin@petermello.net>
 *
 * SPDX-License-Identifier: MIT OR MPL-2.0
 */
    show_source('index.php');
